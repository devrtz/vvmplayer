/* vvmplayer-window.c
 *
 * Copyright 2021 Chris Talbot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vvmplayer-settings.h"

struct _VvmSettings
{
  GObject     parent_instance;

  GtkSettings *gtk_settings;

  //Modem Manager Settings
  char  *vvm_destination_number;
  char  *vvm_type;
  char  *default_number;
  char  *carrier_prefix;

  //Whether or not to activate VVM
  int    enable_vvm;

  //Status Settings
  char  *provision_status;
  int    mm_available;       //Boolean
  int    service_available;  //Boolean
  int    mailbox_active;     //Boolean

  //Don't need to display this
  char  *country_code;

};

G_DEFINE_TYPE (VvmSettings, vvm_settings, G_TYPE_OBJECT)

void
vvm_settings_set_vvm_enabled (VvmSettings *self, int VVMEnabled)
{
  self->enable_vvm = VVMEnabled;
  if (self->enable_vvm)
    g_debug ("VVM Enabled is set to TRUE");
  else
    g_debug ("VVM Enabled is set to FALSE");

}

int
vvm_settings_get_vvm_enabled (VvmSettings *self)
{
  return self->enable_vvm;
}

void
vvm_settings_set_mm_available (VvmSettings *self, int ModemManagerAvailable)
{
  self->mm_available = ModemManagerAvailable;
  if (self->mm_available)
    g_debug ("Modem Manager available through VVMD");
  else
    g_debug ("Modem Manager not available through VVMD");

}

int
vvm_settings_get_mm_available (VvmSettings *self)
{
  return self->mm_available;
}

void
vvm_settings_set_vvm_type (VvmSettings *self, const char *VVMType)
{
  g_free(self->vvm_type);
  self->vvm_type = g_strdup(VVMType);
  g_debug ("VVM Type is set to %s", self->vvm_type);

}

char
*vvm_settings_get_vvm_type (VvmSettings *self)
{
  return g_strdup(self->vvm_type);
}

void
vvm_settings_set_vvm_destination_number (VvmSettings *self,
                                         const char *VVMDestinationNumber)
{
  g_free(self->vvm_destination_number);
  self->vvm_destination_number = g_strdup(VVMDestinationNumber);
  g_debug ("VVM Destination Number is set to %s", self->vvm_destination_number);

}

char
*vvm_settings_get_vvm_destination_number (VvmSettings *self)
{
  return g_strdup(self->vvm_destination_number);
}

void
vvm_settings_set_vvm_carrier_prefix (VvmSettings *self,
                                     const char *CarrierPrefix)
{
  g_free(self->carrier_prefix);
  self->carrier_prefix = g_strdup(CarrierPrefix);
  g_debug ("VVM Carrier Prefix is set to %s", self->carrier_prefix);

}

char
*vvm_settings_get_vvm_carrier_prefix (VvmSettings *self)
{
  return g_strdup(self->carrier_prefix);
}

void
vvm_settings_set_vvm_default_number (VvmSettings *self,
                                     const char *DefaultModemNumber)
{

  g_free(self->default_number);
  self->default_number = g_strdup(DefaultModemNumber);

  g_debug ("VVM Modem Default Number is set to %s", self->default_number);

}

char
*vvm_settings_get_vvm_default_number (VvmSettings *self)
{
  return g_strdup(self->default_number);
}

void
vvm_settings_set_vvm_provision_status (VvmSettings *self,
                                       const char *ProvisionStatus)
{
  g_free(self->provision_status);
  self->provision_status = g_strdup(ProvisionStatus);
  g_debug ("VVM ProvisionStatus is set to %s", self->provision_status);

}

char
*vvm_settings_get_vvm_provision_status (VvmSettings *self)
{
  return g_strdup(self->provision_status);
}

void
vvm_settings_set_service_available (VvmSettings *self, int ServiceAvailable)
{
  self->service_available = ServiceAvailable;
  if (self->service_available)
    g_debug ("VVMD Service available");
  else
    g_debug ("VVMD Service not available");

}

int
vvm_settings_get_service_available (VvmSettings *self)
{
  return self->service_available;
}

void
vvm_settings_set_vvm_country_code (VvmSettings *self,
                                   const char *CountryCode)
{
  g_free(self->country_code);
  self->country_code = g_strdup(CountryCode);
  g_debug ("VVM Modem Country Code is set to %s", self->country_code);

}

void
vvm_settings_set_mailbox_active (VvmSettings *self, int MailboxActive)
{
  self->mailbox_active = MailboxActive;
  if (self->mailbox_active)
    g_debug ("VVM Mailbox active");
  else
    g_debug ("VVM Mailbox not active");

}

int
vvm_settings_get_mailbox_active (VvmSettings *self)
{
  return self->mailbox_active;
}


static void
vvm_settings_constructed (GObject *object)
{
  //VvmSettings *self = (VvmSettings *)object;

  G_OBJECT_CLASS (vvm_settings_parent_class)->constructed (object);

}

static void
vvm_settings_finalize (GObject *object)
{
  VvmSettings *self = (VvmSettings *)object;

  g_object_unref (self->gtk_settings);
  g_free(self->vvm_destination_number);
  g_free(self->vvm_type);
  g_free(self->default_number);
  g_free(self->carrier_prefix);
  g_free(self->provision_status);

  G_OBJECT_CLASS (vvm_settings_parent_class)->finalize (object);
}

static void
vvm_settings_class_init (VvmSettingsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = vvm_settings_constructed;
  object_class->finalize = vvm_settings_finalize;
}

void
vvm_settings_load_mm_defaults (VvmSettings *self)
{
  self->mm_available = FALSE;
  self->enable_vvm = FALSE;
  g_free(self->vvm_destination_number);
  self->vvm_destination_number = g_strdup("Destination Number invalid");
  g_free(self->vvm_type);
  self->vvm_type = g_strdup("Unknown");
  g_free(self->default_number);
  self->default_number = g_strdup("NULL");
  g_free(self->provision_status);
  self->provision_status = g_strdup("Unknown");
  g_free(self->carrier_prefix);
  self->carrier_prefix = g_strdup("Carrier Prefix invalid");
}

void
vvm_settings_load_service_defaults (VvmSettings *self)
{
  self->mailbox_active = FALSE;
  g_free(self->country_code);
  self->country_code = g_strdup("Country Code invalid");
}

static void
vvm_settings_init (VvmSettings *self)
{
  self->gtk_settings = gtk_settings_get_default ();
}

VvmSettings *
vvm_settings_get_default (void)
{
  static VvmSettings *self;

  if (!self)
    {
      self = g_object_new (VVM_TYPE_SETTINGS, NULL);
      g_object_add_weak_pointer (G_OBJECT (self), (gpointer *)&self);
      vvm_settings_load_mm_defaults (self);
      vvm_settings_load_service_defaults (self);
    }
  return self;
}
